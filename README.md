# Python Script to Split a PDF into two pieces and combine

`pdf_split_odds_evens.py` splits the PDF into two PDFs. First PDF contains all the odd pages, second PDF contains all the even pages.

`pdf_merge_odds_evens.py` merges two PDFs into a single PDF (basically the opposite of `pdf_split_odds_evens.py`)

## Installation

The scripts require Python3 and the PyPDF2 library to work.

#### Python

First install Python3

##### Mac OSX

Use https://brew.sh/ as per screenshots below. Once Brew is installed enter `brew install python3`)

![Brew Install Screenshot](/inc/brew_install.png)

If using OSX and you already have Brew installed, update to the latest Python3 version with `brew upgrade python3`

##### Windows

If using Windows, download and install Python from https://www.python.org/ 

#### PyPDF2

Once Python3 is installed, install PyPDF2 using command `python3 -m pip install pypdf2`

![PyPDF2 Install Screenshot](/inc/py2pdf_install.png)

#### Scripts

Download the [repository](https://bitbucket.org/nickheaphy/pdf-split-and-merge-2/get/master.zip) to a folder (or if you have git installed use the command line to clone `git clone https://bitbucket.org/nickheaphy/pdf-split-and-merge-2.git`)

## Using the scripts

### Splitting the PDF

`cd` to the folder containing the scripts.

Call the split script using `python3 pdf_split_odds_evens.py <pdf2split.pdf>` (where <pdf2split.pdf> is the file and path to the PDF file. This will create two file in the same directory as the source file called odd_pdf2split.pdf and even_pdf2split.pdf.

![Split Screenshot](/inc/split_sample.png)

### Merging the PDF

Call the merge script using `python3 pdf_merge_odds_evens.py <odd_page_pdf.pdf> <even_page_pdf.pdf>` (where the odd_page_pdf.pdf is the odd pages of the PDF to merge and even_page_pdf.pdf is the even pages). This will create a new file merge_odd_page_pdf_even_page_pdf.pdf

![Merge Screenshot](/inc/merge_sample.png)

# Warning

Use at own risk. Only limited testing has been done! Please carefully check resulting data for any problems. Note that the PDF library may only handle PDF 1.3 files, so you may need to convert prior to processing.

Please optimise and/or preflight in Acrobat after merging.