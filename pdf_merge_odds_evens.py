# Quick hack.
# Merges two PDF files

# Untested - use at own risk.

from PyPDF2 import PdfFileWriter, PdfFileReader, PdfFileMerger
import os
import sys

def main(pdf_1, pdf_2):

    __pdf1folder__, __pdf1file__ = os.path.split(os.path.abspath(pdf_1))
    __pdf2folder__, __pdf2file__ = os.path.split(os.path.abspath(pdf_2))

    print("Odd Pages from file: %s" % __pdf1file__)
    print("Even Pages from file: %s" % __pdf2file__)

    writer = PdfFileWriter()

    infile1 = PdfFileReader(open(pdf_1, 'rb'))
    infile2 = PdfFileReader(open(pdf_2, 'rb'))

    # Check that the page numbers are the same
    if infile1.getNumPages() != infile2.getNumPages():
        print("Sorry, the two PDF files need to have the same number of pages")
        exit(1)
    
    print("Merging Pages from both files:")
    for i in range(0, infile1.getNumPages()):
        print("%s" % str(i+1), end=" ")
        # get the odd page
        oddpage = infile1.getPage(i)
        writer.addPage(oddpage)
        # get the even page
        evenpage = infile2.getPage(i)
        writer.addPage(evenpage)
    
    print("")
    # and save the writer
    newfilename = "merge_%s_%s" % (__pdf1file__.split('.')[0], __pdf2file__)
    print("Writing %s" % newfilename)
    with open(os.path.join(__pdf1folder__,newfilename), 'wb') as f:
        writer.write(f)
    
    print("Done. You may need to preflight the resulting PDF before printing")
    


if __name__ == "__main__":
# Check the passed parameters
    if len(sys.argv) != 3:
        print("Need to pass the two PDF file to merge on the commandline")
        exit(1)
    
    if sys.version_info[0] != 3:
        print("Sorry, needs Python 3 to handle Unicode ACL/CSV files")
        exit(1)
    
    if not os.path.isfile(sys.argv[1]):
        print("Can't open the PDF file %s" % sys.argv[1])
        exit(1)

    if not os.path.isfile(sys.argv[2]):
        print("Can't open the PDF file %s" % sys.argv[2])
        exit(1)

    print("Note: You may need to save as a PDF 1.3 file before processing")
    main(sys.argv[1], sys.argv[2])
