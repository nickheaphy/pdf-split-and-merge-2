# Quick hack.
# Splits a PDF and into odds and evens

# Untested - use at own risk.

from PyPDF2 import PdfFileWriter, PdfFileReader, PdfFileMerger
import os
import sys

def main(pdf_to_open):

    __pdffolder__, __pdffile__ = os.path.split(os.path.abspath(sys.argv[1]))

    infile = PdfFileReader(open(pdf_to_open, 'rb'))
    number_pages = infile.getNumPages()
    print("Found %s pages..." % number_pages)

    # Split out the odd pages
    newfilename = "odd_%s" % __pdffile__
    print("Splitting out the odd pages to %s" % newfilename)
    outfile = PdfFileWriter()
    for i in range(0, number_pages, 2):
        p = infile.getPage(i)
        outfile.addPage(p)
    with open(os.path.join(__pdffolder__,newfilename), 'wb') as f:
        outfile.write(f)

     # Split out the even pages
    newfilename = "even_%s" % __pdffile__
    print("Splitting out the even pages to %s" % newfilename)
    outfile = PdfFileWriter()
    for i in range(1, number_pages, 2):
        p = infile.getPage(i)
        outfile.addPage(p)
    with open(os.path.join(__pdffolder__,newfilename), 'wb') as f:
        outfile.write(f)


if __name__ == "__main__":
# Check the passed parameters
    if len(sys.argv) != 2:
        print("Need to pass the PDF file on the commandline")
        exit(1)
    
    if sys.version_info[0] != 3:
        print("Sorry, needs Python 3 to handle Unicode ACL/CSV files")
        exit(1)
    
    if not os.path.isfile(sys.argv[1]):
        print("Can't open the PDF file")
        exit(1)

    print("Note: You may need to save as a PDF 1.3 file before processing")
    main(sys.argv[1])
